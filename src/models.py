from base_classes import BaseModel


class ModelWithMeasure(BaseModel):
    """
    Модель с размерностями, 16 параметров
    """
    
    A = None
    B = None
    delt = None
    c0 = None
    c1 = None
    c2 = None
    c3 = None
    c4 = None
    nu = None
    h = None
    sigma = None
    etta = None
    mu = None
    m = None
    gamma = None
    betta = None
    
    def __init__(
            self,
            A, B, delt, c0,
            c1, c2, c3, c4,
            nu, h, sigma, etta, 
            mu, m, gamma, betta,
        ):
        """
        TODO
        """
        
        self.A = A
        self.B = B
        self.delt = delt
        self.c0 = c0
        self.c1 = c1
        self.c2 = c2
        self.c3 = c3
        self.c4 = c4
        self.nu = nu
        self.h = h
        self.sigma = sigma
        self.etta = etta
        self.mu = mu
        self.m = m
        self.gamma = gamma
        self.betta = betta
    
    def get_right_part(self, t, y):
        """
        TODO
        """
        
        c, u, v = y
        y_0 = self.A*self.c0*u/(c+self.c0)-u*c*self.delt/(c+self.c2)-self.nu*c*v/(c+self.c3)-c*self.m
        y_1 = (self.B*c/(c+self.c1)-self.gamma*u)*u-self.betta*u*v/(u+self.h)-self.sigma*u
        y_2 = self.etta*c**2/(c**2+self.c4**2)*self.betta*u*v/(u+self.h)-self.mu*v
        
        return [y_0, y_1, y_2]
    
    
class ModelWithMeasureWithoutOxy(BaseModel):
    """
    Модель с размерностями, 16 параметров
    """
    
    A = None
    B = None
    delt = None
    c0 = None
    c1 = None
    c2 = None
    c3 = None
    c4 = None
    nu = None
    h = None
    sigma = None
    etta = None
    mu = None
    m = None
    gamma = None
    betta = None
    c_0 = None
    
    def __init__(
            self,
            A, B, delt, c0,
            c1, c2, c3, c4,
            nu, h, sigma, etta, 
            mu, m, gamma, betta, c_0
        ):
        """
        TODO
        """
        
        self.A = A
        self.B = B
        self.delt = delt
        self.c0 = c0
        self.c1 = c1
        self.c2 = c2
        self.c3 = c3
        self.c4 = c4
        self.nu = nu
        self.h = h
        self.sigma = sigma
        self.etta = etta
        self.mu = mu
        self.m = m
        self.gamma = gamma
        self.betta = betta
        self.c_0 = c_0
    
    def get_right_part(self, t, y):
        """
        TODO
        """
        
        c, u, v = y
        y_0 = self.A*self.c0*u/(c+self.c0)-u*c*self.delt/(c+self.c2)-self.nu*c*v/(c+self.c3)-c*self.m
        y_1 = (self.B*c/(c+self.c1)-self.gamma*u)*u-self.betta*u*v/(u+self.h)-self.sigma*u
        y_2 = self.etta*c**2/(c**2+self.c4**2)*self.betta*u*v/(u+self.h)-self.mu*v
        
        return [y_0, y_1, y_2]
    
    
class ModelWithMeasureWithoutOxyFixedParameters(BaseModel):
    """
    Модель с размерностями, 16 параметров
    """
    
    A = None
    B = None
    delt = None
    c0 = None
    c1 = None
    c2 = None
    c3 = None
    c4 = None
    nu = None
    h = None
    sigma = None
    etta = None
    mu = None
    m = None
    gamma = None
    betta = None
    c_0 = None
    
    def __init__(
            self,
            A, B, delt, c0,
            c1, c2, c3, c4,
            nu, sigma, m, c_0
        ):
        """
        TODO
        """
        
        self.A = A
        self.B = B
        self.delt = delt
        self.c0 = c0
        self.c1 = c1
        self.c2 = c2
        self.c3 = c3
        self.c4 = c4
        self.nu = nu
        self.h = 0.23
        self.sigma = sigma
        self.etta = 1.06
        self.mu = 0.15
        self.m = m
        self.gamma = 0.48
        self.betta = 0.24
        self.c_0 = c_0
    
    def get_right_part(self, t, y):
        """
        TODO
        """
        
        c, u, v = y
        y_0 = self.A*self.c0*u/(c+self.c0)-u*c*self.delt/(c+self.c2)-self.nu*c*v/(c+self.c3)-c*self.m
        y_1 = (self.B*c/(c+self.c1)-self.gamma*u)*u-self.betta*u*v/(u+self.h)-self.sigma*u
        y_2 = self.etta*c**2/(c**2+self.c4**2)*self.betta*u*v/(u+self.h)-self.mu*v
        
        return [y_0, y_1, y_2]
