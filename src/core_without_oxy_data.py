from conditions import DirectTaskCondition, InverseTaskCondition
from models import ModelWithMeasure, ModelWithMeasureWithoutOxy
from solvers import DirectTaskSolverRK45, InverseTaskSolverDE
from shower import Shower
from residual_functions import residual_function, residual_function_without_oxy

import time


def main():
    initial_data = [0.5, 0.3, 0.02]
    left_time_border = 0
    right_time_border = 365
    delta_time_grid = 7
    
    parameters = [8, 1, 0.25, 0.1, 
                  0.3, 0.4, 0.1, 0.1, 
                  0.1, 0.25, 0.25, 1, 
                  0.15, 0.5, 0.5, 0.25]
    
    bounds = [(4, 12), (0.5, 1.5), (0.125, 0.375), (0.05, 0.15),
              (0.15, 0.45), (0.2, 0.6), (0.05, 0.15), (0.05, 0.15),
              (0.05, 0.15), (0.125, 0.375), (0.125, 0.375), (0.5, 1.5),
              (0.075, 0.225), (0.25, 0.75), (0.25, 0.75), (0.125, 0.375)]
    
    bounds_with_oxy = [(4, 12), (0.5, 1.5), (0.125, 0.375), (0.05, 0.15),
              (0.15, 0.45), (0.2, 0.6), (0.05, 0.15), (0.05, 0.15),
              (0.05, 0.15), (0.125, 0.375), (0.125, 0.375), (0.5, 1.5),
              (0.075, 0.225), (0.25, 0.75), (0.25, 0.75), (0.125, 0.375), (0, 2)]
    
    max_iter = 1000
    popsize = 150
    seed = 1
    
    direct_task_condition = DirectTaskCondition(
        initial_data,
        left_time_border,
        right_time_border,
        delta_time_grid,
    )
    
    model_with_measure = ModelWithMeasure(*parameters)
    
    direct_solver = DirectTaskSolverRK45(model_with_measure)
    
    solution = direct_solver.get_solution(direct_task_condition)
    
    Shower.show_solution(solution)
    
    inverse_solutions = []
     
    for seed in [15, 30, 45, 60, 75]:
    # for seed in [167]:
        
        # inverse_task_condition = InverseTaskCondition(
        #     solution,
        #     left_time_border,
        #     right_time_border,
        #     delta_time_grid,
        #     bounds,
        #     max_iter,
        #     popsize,
        #     seed,
        # )
        inverse_task_condition = InverseTaskCondition(
            solution,
            left_time_border,
            right_time_border,
            delta_time_grid,
            bounds_with_oxy,
            max_iter,
            popsize,
            seed,
        )
        
        # inverse_task_solver = InverseTaskSolverDE(ModelWithMeasure, residual_function)
        inverse_task_solver = InverseTaskSolverDE(ModelWithMeasureWithoutOxy, residual_function_without_oxy)
        
        
        
        start = time.time()
        print('Start inverse task solution.\n')
        
        inverse_solution = inverse_task_solver.get_solution(inverse_task_condition)
        
        execution_time = round((time.time()-start)/60, 2)
        print(f'Start inverse task solution.\n Execution time: {execution_time}min' )
        inverse_solutions.append(inverse_solution)
    
    return inverse_solutions, solution


if __name__=="__main__":
    result, data = main()
    
    # initial_data = [0.5, 0.3, 0.02]
    # left_time_border = 0
    # right_time_border = 365
    # delta_time_grid = 7
    
    # direct_task_condition = DirectTaskCondition(
    #     initial_data,
    #     left_time_border,
    #     right_time_border,
    #     delta_time_grid,
    # )
    
    # model = ModelWithMeasureWithoutOxy(*result[0].x)
    
    # direct_task_condition.initial_data[0] = model.c_0
    
    # direct_solver = DirectTaskSolverRK45(model)
    
    # solution = direct_solver.get_solution(direct_task_condition)
    
    # Shower.show_solution_and_data(solution, data)