from scipy.integrate import solve_ivp
from scipy import optimize

from base_classes import (
    BaseDirectTaskSolver,
    BaseInverseTaskSolver,
)
from conditions import (
    DirectTaskCondition,
    InverseTaskCondition,
)


class DirectTaskSolverRK45(BaseDirectTaskSolver):
    """
    TODO
    """
    
    def __init__(self, model):
        """
        TODO
        """
        
        self.model = model
    
    def get_solution(
        self,
        dtc: DirectTaskCondition,
    ):
        """
        TODO
        """
        
        time_interval = [dtc.left_time_border, dtc.right_time_border]
        
        solution = solve_ivp(
            self.model.get_right_part,
            time_interval,
            dtc.initial_data,
            method="Radau",
            t_eval=dtc.time_grid,
        )
        
        return solution
    
    
class InverseTaskSolverDE(BaseInverseTaskSolver):
    """
    TODO
    """
    
    model_object = None
    residual_function = None
    
    def __init__(self, model_object, residual_function):
        """
        TODO
        """
        
        self.model_object = model_object
        self.residual_function = residual_function
        
    def get_solution(
        self,
        itc: InverseTaskCondition,
    ):
        """
        TODO
        """
        
        args = (
            self.model_object, 
            itc,
        )
        if itc.mutation:
            result = optimize.differential_evolution(
                self.residual_function,
                itc.bounds,
                args=args,
                maxiter=itc.max_iter,
                polish=False,
                mutation=itc.mutation,
                popsize=itc.popsize,
                updating='deferred',
                workers=-1,
                seed=itc.seed,
                disp=True,
            )
        else:
            result = optimize.differential_evolution(
                self.residual_function,
                itc.bounds,
                args=args,
                maxiter=itc.max_iter,
                polish=False,
                popsize=itc.popsize,
                updating='deferred',
                workers=-1,
                seed=itc.seed,
                disp=True,
            )
        
        return result
