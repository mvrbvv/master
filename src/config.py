DEPTH = '0-10'
SEED = 167

# BOUNDS = [(0.001, 100), (0.001, 100), (0.001, 100), (0.001, 100),
#           (0.001, 100), (0.001, 100), (0.001, 100), (0.001, 100),
#           (0.001, 100), (0.001, 100), (0.001, 100), (0.001, 100),
#           (0.001, 100), (0.001, 100), (0.001, 100), (0.001, 100), (0.001, 2)]

BOUNDS = [(0.001, 500), (0.001, 500), (0.001, 500), (0.001, 500),
          (0.001, 500), (0.001, 500), (0.001, 500), (0.001, 500),
          (0.001, 500), (0.001, 500), (0.001, 500), (0.001, 1),
          (0.001, 500), (0.001, 500), (0.001, 500), (0.001, 500), (0.001, 500)]

# BOUNDS = [(0.0001, 1000), (0.0001, 1000), (0.0001, 1000), (0.0001, 1000),
#           (0.0001, 1000), (0.0001, 1000), (0.0001, 1000), (0.0001, 1000),
#           (0.0001, 1000), (0.0001, 1000), (0.0001, 1000), (0.0001, 1),
#           (0.0001, 1000), (0.0001, 1000), (0.0001, 1000), (0.0001, 1000), (0.0001, 1000)]

# BOUNDS = [(0.0001, 100), (0.0001, 100), (0.0001, 100), (0.0001, 100),
#           (0.0001, 100), (0.0001, 100), (0.0001, 100), (0.0001, 100),
#           (0.0001, 100), (0.0001, 100), (0.0001, 100), (0.0001, 1),
#           (0.0001, 100), (0.0001, 100), (0.0001, 100), (0.0001, 100), (0.0001, 200)]

MAX_ITER = 2000
POPSIZE = 500
MUTATION = None
