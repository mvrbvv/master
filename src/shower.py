import matplotlib.pyplot as plt


class Shower:
    """
    TODO
    """
    
    def show_solution(self, solution: dict, title='DEFAULT_TITLE'):
        """
        TODO
        """
        
        fig, gr = plt.subplots(figsize=(16, 8))

        gr.plot(solution['t'], solution['y'][0, :], '-o', color='blue', label='C(OXY)')
        gr.plot(solution['t'], solution['y'][1, :], '-o', color='green', label='U(PHYTO)')
        gr.plot(solution['t'], solution['y'][2, :], '-o', color='black', label='V(ZOO)')

        gr.legend(loc='upper right')
        gr.grid()
        gr.set_xlabel('Time')
        gr.set_ylabel('C, U, V')
        gr.set_title(title)
        
        return fig, gr
        
    def show_solution_and_data(self, solution: dict, data):
        """
        TODO
        """
        
        fig, gr = plt.subplots(figsize=(16, 8))

        gr.plot(solution['t'], solution['y'][0, :], '-o', color='darkblue', label='C(OXY) - solution')
        gr.plot(solution['t'], solution['y'][1, :], '-o', color='darkgreen', label='U(PHYTO) - solution')
        gr.plot(solution['t'], solution['y'][2, :], '-o', color='black', label='V(ZOO) - solution')

        gr.plot(data['t'], data['y'][0, :], '-o', color='blue', alpha=0.5, label='C(OXY) - data')
        gr.plot(data['t'], data['y'][1, :], '-o', color='green', alpha=0.5, label='U(PHYTO) - data')
        gr.plot(data['t'], data['y'][2, :], '-o', color='gray', alpha=0.5, label='V(ZOO) - data')

        gr.legend(loc='upper right')
        gr.grid()
        gr.set_xlabel('Time')
        gr.set_ylabel('C, U, V')

        return fig, gr

    def show_solution_and_data_without_oxy(self, solution: dict, data):
        """
        TODO
        """

        fig, gr = plt.subplots(figsize=(16, 8))

        gr.plot(solution['t'], solution['y'][0], '-o', color='darkblue', label='C(OXY) - solution')
        gr.plot(solution['t'], solution['y'][1], '-o', color='darkgreen', label='U(PHYTO) - solution')
        gr.plot(solution['t'], solution['y'][2], '-o', color='black', label='V(ZOO) - solution')

        gr.plot(data['t'], data['y'][1], '-o', color='green', alpha=0.5, label='U(PHYTO) - data')
        gr.plot(data['t'], data['y'][2], '-o', color='gray', alpha=0.5, label='V(ZOO) - data')

        gr.legend(loc='upper right')
        gr.grid()
        gr.set_xlabel('Time')
        gr.set_ylabel('C, U, V')

        return fig, gr

    def show_solution_and_data_without_oxy_split(self, solution: dict, data):
        """
        TODO
        """

        fig, gr = plt.subplots(3, 1, figsize=(16, 12))

        gr[0].plot(
            solution['t'],
            solution['y'][0],
            '-o',
            color='darkblue',
            label='c(oxy) - solution',
        )

        gr[0].legend()
        gr[0].grid()
        gr[0].set_xlabel('time')
        gr[0].set_ylabel('c')


        gr[1].plot(
            solution['t'],
            solution['y'][1],
            '-o',
            color='darkgreen',
            label='u(phyto) - solution',
        )

        gr[1].plot(
            data['t'],
            data['y'][1],
            marker='o',
            linestyle='--',
            color='green',
            alpha=0.75,
            label='u(phyto) - data',
        )

        gr[1].legend()
        gr[1].grid()
        gr[1].set_xlabel('time')
        gr[1].set_ylabel('u')


        gr[2].plot(
            solution['t'],
            solution['y'][2],
            '-o',
            color='black',
            label='v(zoo) - solution',
        )

        gr[2].plot(
            data['t'],
            data['y'][2],
            marker='o',
            linestyle='--',
            color='gray',
            alpha=0.75,
            label='v(zoo) - data',
        )

        gr[2].legend()
        gr[2].grid()
        gr[2].set_xlabel('time')
        gr[2].set_ylabel('v')