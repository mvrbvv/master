from abc import ABC, abstractmethod


class BaseTaskCondition(ABC):
    """
    Абстрактный класс для хранения условий решения задач
    """

    @abstractmethod
    def __str__(self):
        return None


class BaseModel(ABC):
    """
    Абстрактный класс для модели дифференциальных уравнений
    """

    @abstractmethod
    def get_right_part(self):
        """
        Метод получения значения правой части системы
        дифференциальных уравнений, вычисленной в точке
        """
        
        return None
    

class BaseDirectTaskSolver(ABC):
    """
    Абстрактный класс решателя прямой задачи для системы 
    дифференциальных уравнений
    """
    
    @abstractmethod
    def get_solution(self):
        """
        Метод получения решения прямой задачи для системы 
        дифференциальных уравнений
        """
        
        return None
    
    
class BaseInverseTaskSolver(ABC):
    """
    Абстрактный класс решателя обратной задачи для системы 
    дифференциальных уравнений
    """
    
    @abstractmethod
    def get_solution(self):
        """
        Метод получения решения обратной задачи для системы
        дифференциальных уравнений
        """
        
        return None
    