from solvers import DirectTaskSolverRK45
from conditions import DirectTaskCondition


def residual_function(parameters, model_object, itc):
    
    model = model_object(*parameters)
    direct_task_solver = DirectTaskSolverRK45(model)
    
    direct_task_condition = DirectTaskCondition(
        itc.initial_data,
        itc.left_time_border,
        itc.right_time_border,
        itc.time_grid,
    )
        
    try:
        
        solution = direct_task_solver.get_solution(direct_task_condition)
        
    except:
        # print('ERROR')
        return 10_000_000
    
    result = 0
    
    if solution.y.shape[1] != itc.data.y.shape[1]:
        # print('ERROR')
        return 10_000_000
    
    for i in range(itc.data.y.shape[0]):
        result += sum((itc.data.y[i] - solution.y[i])**2)
        
    return result


def residual_function_without_oxy(parameters, model_object, itc):
    
    model = model_object(*parameters)
    direct_task_solver = DirectTaskSolverRK45(model)
    
    direct_task_condition = DirectTaskCondition(
        itc.initial_data,
        itc.left_time_border,
        itc.right_time_border,
        itc.time_grid,
    )
    
    direct_task_condition.initial_data[0] = model.c_0
    
    oxy_upper_border = max(itc.data['y'][0]) + 0.25*abs(max(itc.data['y'][0]))
    oxy_lower_border = min(itc.data['y'][0]) - 0.25*abs(min(itc.data['y'][0]))
    
    try:
        
        solution = direct_task_solver.get_solution(direct_task_condition)
    
    except:
        # print('ERROR')
        return 100_000_000
    
    result = 0
    solution_length = len(solution['y'][-1])
    data_length = len(itc.data['y'][-1])
    
    if solution_length != data_length:
        # print(f'ERROR, solution lenght({solution_length}) not equal data length ({data_length})')
        return 100_000_000
    
    for i in range(solution['y'].shape[1]):
        if (solution['y'][0][i] > oxy_upper_border) or (solution['y'][0][i] < oxy_lower_border):
            return 100_000_000
    
    for i in [1, 2]:
        result += sum((itc.data['y'][i] - solution['y'][i])**2)
    
    return result


def residual_function_without_oxy_reg_linear(parameters, model_object, itc, alpha=100):
    model = model_object(*parameters)
    direct_task_solver = DirectTaskSolverRK45(model)

    direct_task_condition = DirectTaskCondition(
        itc.initial_data,
        itc.left_time_border,
        itc.right_time_border,
        itc.time_grid,
    )

    direct_task_condition.initial_data[0] = model.c_0

    oxy_upper_border = max(itc.data['y'][0]) + 0.25 * abs(max(itc.data['y'][0]))
    oxy_lower_border = min(itc.data['y'][0]) - 0.25 * abs(min(itc.data['y'][0]))

    try:

        solution = direct_task_solver.get_solution(direct_task_condition)

    except:
        # print('ERROR')
        return 10_000_000

    result = 0
    solution_length = len(solution['y'][-1])
    data_length = len(itc.data['y'][-1])

    if solution_length != data_length:
        # print(f'ERROR, solution lenght({solution_length}) not equal data length ({data_length})')
        return 10_000_000

    for i in range(solution['y'].shape[1]):
        if (solution['y'][0][i] > oxy_upper_border) or (solution['y'][0][i] < oxy_lower_border):
            return 10_000_000

    for i in [1, 2]:
        result += sum((itc.data['y'][i] - solution['y'][i]) ** 2)

    for i in range(solution_length-10):
        result -= abs(solution['y'][1][i + 6] - solution['y'][1][i + 5]) * alpha
        result -= abs(solution['y'][2][i + 6] - solution['y'][2][i + 5]) * alpha

    return result
