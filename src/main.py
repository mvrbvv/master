import numpy as np
import matplotlib.pyplot as plt
import time
import logging

from typing import Optional, List
from abc import ABC, abstractmethod
from scipy.integrate import solve_ivp
from scipy import optimize


class BaseTaskCondition(ABC):
    """
    Абстрактный класс для хранения условий решения задач
    """

    @abstractmethod
    def __str__():
        return None


class BaseModel(ABC):
    """
    TODO
    """
    
    @abstractmethod
    def get_right_part(self, t, y):
        """
        TODO
        """
        
        return None
    

class BaseDirectTaskSolver(ABC):
    """
    TODO
    """
    
    model=None
    
    @abstractmethod
    def get_solution():
        """
        TODO
        """
        
        return None
    
    

class BaseInverseTaskSolver(ABC):
    """
    TODO
    """
    
    @abstractmethod
    def get_solution(
            self,
            data,
            left_time_border,
            right_time_border,
            time_grid,
            bounds,
        ):
        """
        TODO
        """
        
        return None
    
    
class DirectTaskCondition(BaseTaskCondition):
    """
    TODO
    """
    
    initial_data: List
    left_time_border: int
    right_time_border: int
    delta_time_grid: int
    time_grid: List
    
    def __init__(
            self, 
            initial_data, 
            left_time_border, 
            right_time_border, 
            delta_time_grid=1,
        ):
        """
        TODO
        """
        
        self.initial_data = initial_data
        self.left_time_border = left_time_border
        self.right_time_border = right_time_border
        self.delta_time_grid = delta_time_grid
        self.time_grid = list(np.arange(left_time_border, right_time_border, delta_time_grid))
        
    def __str__(self):
        return f'''
            initial_data: {self.initial_data},
            left_time_border: {self.left_time_border},
            right_time_border: {self.right_time_border},
            delta_time_grid: {self.delta_time_grid}
            time_grid: {self.time_grid}
            '''
            
    
class InverseTaskCondition(BaseTaskCondition):
    """
    TODO
    """
    
    data: List
    left_time_border: int
    right_time_border: int
    delta_time_grid: int
    time_grid: List
    initial_data: List
    bounds: List
    
    max_iter: int
    popsize: int
    
    
    def __init__(
            self, 
            data, 
            left_time_border, 
            right_time_border, 
            delta_time_grid,
            bounds,
            max_iter=5,
            popsize=150,
        ):
        """
        TODO
        """
        
        self.data = data
        self.left_time_border = left_time_border
        self.right_time_border = right_time_border
        self.delta_time_grid = delta_time_grid
        self.max_iter = max_iter
        self.popsize = popsize
        self.bounds = bounds
        
        self.time_grid = list(np.arange(left_time_border, right_time_border, delta_time_grid))
        self.initial_data = [data.y[i][0] for i in range(data.y.shape[0])]
        
    def __str__(self):
        return f'''
            data: {self.data},
            left_time_border: {self.left_time_border},
            right_time_border: {self.right_time_border},
            delta_time_grid: {self.delta_time_grid},
            time_grid: {self.time_grid},
            initial_data: {self.initial_data},
            bounds: {self.bounds},\n
            max_iter: {self.max_iter},
            popsize: {self.popsize}
            '''
    
    

class ModelWithMeasure(BaseModel):
    """
    Модель с размерностями, 16 параметров
    """
    
    A = None
    B = None
    delt = None
    c0 = None
    c1 = None
    c2 = None
    c3 = None
    c4 = None
    nu = None
    h = None
    sigma = None
    etta = None
    mu = None
    m = None
    gamma = None
    betta = None
    
    def __init__(
            self,
            A, B, delt, c0,
            c1, c2, c3, c4,
            nu, h, sigma, etta, 
            mu, m, gamma, betta,
        ):
        """
        TODO
        """
        
        self.A = A
        self.B = B
        self.delt = delt
        self.c0 = c0
        self.c1 = c1
        self.c2 = c2
        self.c3 = c3
        self.c4 = c4
        self.nu = nu
        self.h = h
        self.sigma = sigma
        self.etta = etta
        self.mu = mu
        self.m = m
        self.gamma = gamma
        self.betta = betta
    
    def get_right_part(self, t, y):
        """
        TODO
        """
        
        c, u, v = y
        y_0 = self.A*self.c0*u/(c+self.c0)-u*c*self.delt/(c+self.c2)-self.nu*c*v/(c+self.c3)-c*self.m
        y_1 = (self.B*c/(c+self.c1)-self.gamma*u)*u-self.betta*u*v/(u+self.h)-self.sigma*u
        y_2 = self.etta*c**2/(c**2+self.c4**2)*self.betta*u*v/(u+self.h)-self.mu*v
        
        return [y_0, y_1, y_2]
    
    
class DirectTaskSolverRK45(BaseDirectTaskSolver):
    """
    TODO
    """
    
    def __init__(self, model):
        """
        TODO
        """
        
        self.model = model
    
    def get_solution(
            self,
            dtc: DirectTaskCondition,
        ):
        """
        TODO
        """
        
        time_interval = [dtc.left_time_border, dtc.right_time_border]
        
        solution = solve_ivp(
            self.model.get_right_part,
            time_interval,
            dtc.initial_data,
            method = "Radau",
            t_eval = dtc.time_grid,
        )
        
        return solution
    
    
class InverseTaskSolver(BaseInverseTaskSolver):
    """
    TODO
    """
    
    model_object = None
    residual_function = None
    
    def __init__(self, model_object, residual_function):
        """
        TODO
        """
        
        self.model_object = model_object
        self.residual_function = residual_function
        
    def get_solution(
            self,
            itc: InverseTaskCondition,
        ):
        """
        TODO
        """
        
        logger = logging.getLogger()
        logger.setLevel('DEBUG')
        
        start = time.time()
        logger.debug('Start inverse task solution.\n')
        
        
        args = (
            self.model_object, 
            itc,
        )
        
        result = optimize.differential_evolution(
            self.residual_function, 
            itc.bounds, 
            args=args, 
            maxiter=itc.max_iter, 
            polish=True, 
            popsize=itc.popsize, 
            updating='deferred', 
            workers=-1, 
            seed=1, 
            disp=True,
        )
        
        execution_time = round(time.time()-start, 2)/60
        
        logger.debug('End inverse task solution.')
        logger.debug(f'Execution time: {execution_time} min \n')
        
        return result
    

class Shower:
    """
    TODO
    """
    
    def show_solution(solution):
        """
        TODO
        """
        
        fig, gr = plt.subplots(figsize=(16, 8))
        gr.plot(solution.t, solution.y[0, :],'-o', color='blue', label='C(OXY)')
        gr.plot(solution.t, solution.y[1, :],'-o', color='green', label='U(PHYTO)')
        gr.plot(solution.t, solution.y[2, :],'-o', color='black', label='V(ZOO)')
        gr.legend(loc='upper right')
        gr.grid()
        gr.set_xlabel('Time')
        gr.set_ylabel('C, U, V')
        
    def show_solution_and_data(solution, data):
        """
        TODO
        """
        
        fig, gr = plt.subplots(figsize=(16, 8))
        gr.plot(solution.t, solution.y[0, :],'-o', color='darkblue', label='C(OXY) - solution')
        gr.plot(solution.t, solution.y[1, :],'-o', color='darkgreen', label='U(PHYTO) - solution')
        gr.plot(solution.t, solution.y[2, :],'-o', color='black', label='V(ZOO) - solution')
        gr.plot(data.t, data.y[0, :],'-o', color='blue', alpha=0.5, label='C(OXY) - data')
        gr.plot(data.t, data.y[1, :],'-o', color='green', alpha=0.5, label='U(PHYTO) - data')
        gr.plot(data.t, data.y[2, :],'-o', color='gray', alpha=0.5, label='V(ZOO) - data')
        gr.legend(loc='upper right')
        gr.grid()
        gr.set_xlabel('Time')
        gr.set_ylabel('C, U, V')

 
# def residual_function(parameters, model_object, data, left_time_border, right_time_border, delta_time_grid):
def residual_function(parameters, model_object, itc):
    
    model = model_object(*parameters)
    direct_task_solver = DirectTaskSolverRK45(model)
    
    direct_task_condition = DirectTaskCondition(
        itc.initial_data,
        itc.left_time_border,
        itc.right_time_border,
        itc.delta_time_grid,
    )
        
    try:
        
        solution = direct_task_solver.get_solution(direct_task_condition)
        
    except:
        print('ERROR')
        return 10_000_000
    
    result = 0
    
    if solution.y.shape[1] != itc.data.y.shape[1]:
        print('ERROR')
        return 10_000_000
    
    for i in range(itc.data.y.shape[0]):
        result += sum((itc.data.y[i] - solution.y[i])**2)
        
    return result

        
if __name__=='__main__':
    
    initial_data = [0.5, 0.3, 0.02]
    left_time_border = 0
    right_time_border = 365
    delta_time_grid = 7
    
    parameters = [8, 1, 0.25, 0.1, 
                  0.3, 0.4, 0.1, 0.1, 
                  0.1, 0.25, 0.25, 1, 
                  0.15, 0.5, 0.5, 0.25]
    
    bounds = [(4, 12), (0.5, 1.5), (0.125, 0.375), (0.05, 0.15),
              (0.15, 0.45), (0.2, 0.6), (0.05, 0.15), (0.05, 0.15),
              (0.05, 0.15), (0.125, 0.375), (0.125, 0.375), (0.5, 1.5),
              (0.075, 0.225), (0.25, 0.75), (0.25, 0.75), (0.125, 0.375)]
    
    max_iter = 10
    popsize = 150
    
    direct_task_condition = DirectTaskCondition(
        initial_data,
        left_time_border,
        right_time_border,
        delta_time_grid,
    )
    
    model_with_measure = ModelWithMeasure(*parameters)
    
    direct_solver = DirectTaskSolverRK45(model_with_measure)
    
    solution = direct_solver.get_solution(direct_task_condition)
    
    Shower.show_solution(solution)
    
    inverse_task_condition = InverseTaskCondition(
        solution,
        left_time_border,
        right_time_border,
        delta_time_grid,
        bounds,
        max_iter,
        popsize,
    )
    
    inverse_task_solver = InverseTaskSolver(ModelWithMeasure, residual_function)
    
    inverse_solution = inverse_task_solver.get_solution(inverse_task_condition)
    
    
    
    
    
        