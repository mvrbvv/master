import time
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import os
import pickle

from loguru import logger

from conditions import DirectTaskCondition, InverseTaskCondition
from models import ModelWithMeasure, ModelWithMeasureWithoutOxy
from solvers import DirectTaskSolverRK45, InverseTaskSolverDE
from shower import Shower
from residual_functions import residual_function_without_oxy, residual_function_without_oxy_reg_linear

from config import (
    DEPTH,
    SEED,
    MAX_ITER,
    POPSIZE,
    BOUNDS,
    MUTATION,
)


import warnings
warnings.filterwarnings("ignore")

if __name__ == '__main__':

    if not os.path.exists('results'):
        os.makedirs('results')

    folder_name = f'{str(pd.Timestamp.now().round("s"))}'
    if not os.path.exists(f'results/{folder_name}'):
        os.makedirs(f'results/{folder_name}')

    ph = pd.read_excel('2016_phyto_0-50.xlsx')

    ph = ph.rename(columns={'дата': 'date', 'горизонт, м': 'depth', 'численность, млн.кл/л': 'value'})
    ph = ph[['date', 'depth', 'value']].copy()
    ph['depth'] = ph['depth'].astype(str)
    ph = ph.groupby(['date', 'depth'])['value'].mean().unstack().reset_index()
    ph['date'] = pd.to_datetime(ph['date'])
    ph = ph.sort_values(by=['date']).reset_index(drop=True)
    ph = ph.ffill(axis=0)

    ph['0-10_sum'] = ph['0'] + ph['5'] + ph['10']
    ph['10-25_sum'] = ph['10'] + ph['25']
    ph['25-50_sum'] = ph['25'] + ph['50']
    ph['t'] = (ph['date'].dt.date - ph['date'].dt.date.iloc[0]).apply(lambda x: x.days)


    ph = ph.loc[ph['t'].between(131, 221)].reset_index(drop=True).copy()
    ph['t'] -= 131

    zoo = pd.read_excel('2016_zoo.xlsx')

    zoo = zoo.rename(
        columns={
            'слой (м)': 'depth'
        }
    )
    zoo = zoo.loc[:2, :].copy()
    zoo = zoo.set_index('depth').T.reset_index().rename(columns={'index': 'date', ' 10-25': '10-25'})
    zoo['date'] = pd.to_datetime(zoo['date'])
    zoo = zoo.sort_values(by=['date']).reset_index(drop=True)
    zoo['0-10'] /= 10
    zoo['10-25'] /= 10
    zoo['25-50'] /= 10
    zoo['t'] = (zoo['date'].dt.date - zoo['date'].dt.date.iloc[0]).apply(lambda x: x.days)

    zoo = zoo.loc[zoo['t'].between(131, 221)].reset_index(drop=True).copy()
    zoo['t'] -= 131

    fig, ax = plt.subplots(figsize=(16, 6))
    ax.plot(ph['t'], ph[f'{DEPTH}_sum'], label='u (phyto)', marker='o', linestyle='--', color='green')
    ax.plot(zoo['t'], zoo[f'{DEPTH}'], label='v (zoo)', marker='o', linestyle='--', color='black')
    ax.grid()
    ax.set_xlabel('time')
    ax.legend()
    plt.savefig(f'results/{folder_name}/real_data_{DEPTH}_depth_sum.png')

    SOLUTION = {
        't': list(ph['t'].values),
        'y': list(([0.1, 150], list(ph[f'{DEPTH}_sum'].values), list(zoo[f'{DEPTH}'].values)))
    }

    INITIAL_DATA = ['no', SOLUTION['y'][1][0], SOLUTION['y'][2][0]]

    LEFT_TIME_BORDER = SOLUTION['t'][0]
    RIGHT_TIME_BORDER = SOLUTION['t'][-1]
    TIME_GRID = SOLUTION['t']

    RUN_SETTINGS = {
        'GENERAL_PARAMS': {
            'DATA': SOLUTION,
            'DEPTH': DEPTH,
            'INITIAL_DATA': INITIAL_DATA,
            'LEFT_TIME_BORDER': LEFT_TIME_BORDER,
            'RIGHT_TIME_BORDER': RIGHT_TIME_BORDER,
            'TIME_GRID': TIME_GRID,
        },
        'DIFFERENTIAL_EVOLUTION_PARAMS': {
            'SEED': SEED,
            'BOUNDS': BOUNDS,
            'MAX_ITER': MAX_ITER,
            'POPSIZE': POPSIZE,
            'MUTATION': MUTATION,
        },
    }

    with open(f'results/{folder_name}/run_settings.pkl', 'wb') as f:
        pickle.dump(RUN_SETTINGS, f)

    inverse_task_condition = InverseTaskCondition(
        SOLUTION,
        LEFT_TIME_BORDER,
        RIGHT_TIME_BORDER,
        TIME_GRID,
        BOUNDS,
        MAX_ITER,
        POPSIZE,
        SEED,
        MUTATION,
    )

    inverse_task_solver = InverseTaskSolverDE(ModelWithMeasureWithoutOxy, residual_function_without_oxy)

    start = time.time()
    logger.info('Start inverse task solution.')

    inverse_solution = inverse_task_solver.get_solution(inverse_task_condition)

    execution_time = round((time.time() - start) / 60, 2)

    with open(f'results/{folder_name}/inverse_solution.pkl', 'wb') as f:
        pickle.dump(dict(inverse_solution), f)

    logger.info(f'End inverse task solution. Execution time: {execution_time}min')

    result, data = inverse_solution.x.copy(), SOLUTION

    model = ModelWithMeasureWithoutOxy(*result)

    direct_task_condition_new = DirectTaskCondition(
        INITIAL_DATA,
        LEFT_TIME_BORDER,
        RIGHT_TIME_BORDER,
        TIME_GRID,
    )

    direct_task_condition_new.initial_data[0] = model.c_0

    direct_solver = DirectTaskSolverRK45(model)

    direct_solution_by_params = direct_solver.get_solution(direct_task_condition_new)

    shower = Shower()

    shower.show_solution(direct_solution_by_params)
    plt.savefig(f'results/{folder_name}/direct_solution_by_params.png')

    shower.show_solution_and_data_without_oxy(direct_solution_by_params, data)
    plt.savefig(f'results/{folder_name}/direct_solution_by_params_with_data.png')

    shower.show_solution_and_data_without_oxy_split(direct_solution_by_params, data)
    plt.savefig(f'results/{folder_name}/direct_solution_by_params_with_data_split.png')





