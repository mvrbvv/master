import numpy as np

from typing import List

from base_classes import BaseTaskCondition


class DirectTaskCondition(BaseTaskCondition):
    """
    TODO
    """
    
    initial_data: List
    left_time_border: int
    right_time_border: int
    time_grid: List
    
    def __init__(
            self, 
            initial_data, 
            left_time_border, 
            right_time_border,
            time_grid,
    ):
        """
        TODO
        """
        
        self.initial_data = initial_data
        self.left_time_border = left_time_border
        self.right_time_border = right_time_border
        self.time_grid = time_grid
        
    def __str__(self):
        return 'pass'
    #     return f'''
    #         initial_data: {self.initial_data},
    #         left_time_border: {self.left_time_border},
    #         right_time_border: {self.right_time_border},
    #         time_grid: {self.time_grid}
    #         '''
            
            
class InverseTaskCondition(BaseTaskCondition):
    """
    TODO
    """
    
    data: List
    left_time_border: int
    right_time_border: int
    time_grid: List
    initial_data: List
    bounds: List
    seed: int
    max_iter: int
    popsize: int
    mutation: float

    def __init__(
        self,
        data,
        left_time_border,
        right_time_border,
        time_grid,
        bounds,
        max_iter=5,
        popsize=150,
        seed=1,
        mutation=None,
    ):
        """
        TODO
        """
        
        self.data = data
        self.left_time_border = left_time_border
        self.right_time_border = right_time_border
        self.time_grid = time_grid
        self.max_iter = max_iter
        self.popsize = popsize
        self.bounds = bounds
        self.seed = seed
        self.mutation = mutation

        self.initial_data = [data['y'][i][0] for i in range(len(data['y']))]
        
    def __str__(self):
        return 'pass'
    #     return f'''
    #         data: {self.data},
    #         left_time_border: {self.left_time_border},
    #         right_time_border: {self.right_time_border},
    #         time_grid: {self.time_grid},
    #         initial_data: {self.initial_data},
    #         bounds: {self.bounds},\n
    #         max_iter: {self.max_iter},
    #         popsize: {self.popsize}
    #         '''
